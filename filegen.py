lines1 = """
'Twas brillig, and the slithy toves
  Did gyre and gimble in the wabe:
All mimsy were the borogoves,
  And the mome raths outgrabe.
"""

lines2 = """ 
"Beware the Jabberwock, my son!
  The jaws that bite, the claws that catch!
Beware the Jubjub bird, and shun
  The frumious Bandersnatch!"
"""

def tweak_lines(x, add_cr):
    joiner = '\r\n' if add_cr else '\n'
    return joiner.join(x.splitlines())

with open('test1.txt','wb') as f:
    f.write(tweak_lines("These lines have CRLF"+lines1, True))
    f.write(tweak_lines("\n\nThese lines have LF only"+lines2, False))

with open('test2.txt','wb') as f:
    f.write(tweak_lines("These lines have LF only"+lines1, False))
    f.write(tweak_lines("\n\nThese lines have CRLF"+lines2, True))
